(function(){
    $('.js-collapse-trigger').on('click', function() {
        const $this = $(this);
        const $parent = $this.closest('.js-collapse');
        const $inner = $this.siblings('.js-collapse-inner');

        if ($parent.hasClass('collapse-is-opened')) {
            $inner.stop().slideUp(300);
            $parent.removeClass('collapse-is-opened');
        } else {
            $inner.stop().slideDown(300);
            $parent.addClass('collapse-is-opened');
        }
    });
})();