(function(){
    // header scroll
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 10) {
            $('.header').addClass('is-scrolled');
        } else {
            $('.header').removeClass('is-scrolled');
        }
    });

    $('.header__burger').on('click', function() {
        const $wrapper = $(this).closest('.l-wrapper');
        if ($wrapper.hasClass('is-opened')) {
            $wrapper.removeClass('is-opened');
            closeDialog();
        } else {
            $wrapper.addClass('is-opened');
            showDialog();
        }
    });
    $('.profile-header__burger').on('click', function() {
        const $wrapper = $(this).closest('.l-wrapper');
        if ($wrapper.hasClass('is-opened-menu-profile')) {
            $wrapper.removeClass('is-opened-menu-profile');
            closeDialog();
        } else {
            $wrapper.addClass('is-opened-menu-profile');
            showDialog();
        }
    });
})();