(function(){
    if (document.getElementById('paymentDate')) {
        const datePicker = new Lightpick({
            field: document.getElementById('paymentDate'),
            lang: 'ru',
            format: 'DD MMM YYYY'
        });
    }
})();