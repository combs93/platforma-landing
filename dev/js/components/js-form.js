(function(){

    // select
    $('.js-select').each(function() {
        const placeholder = $(this).attr('data-placeholder')
        $(this).select2({
            minimumResultsForSearch: -1,
            placeholder: placeholder,
        });
    });

    $('.js-select-type').select2({
        minimumResultsForSearch: -1,
        templateResult: templateType2,
        templateSelection: templateType2
    });

    function templateType2 (opt) {
        if (!opt.id) {
            return opt.text.toUpperCase();
        }
        var optImage = $(opt.element).attr('data-image');
        var optType = $(opt.element).attr('data-type');
        var optValue = $(opt.element).attr('data-value');
        console.log(optImage);
        if(!optImage){
            return opt.text.toUpperCase();
        } else {
            var $opt = $(
                '<span class="select-type2Wrap">'
                    +
                    '<span class="select-left">' +
                        '<span class="select-img-container">' +
                            '<img src="' + optImage + '" width="24px" />' +
                        '</span>' + opt.text.toUpperCase()
                    +
                    '</span>'
                +
                '</span>'
            );
            return $opt;
        }
    }
})();