(function(){
	$('.js-sidebar-menu-trigger').on('click', function() {
		$(this).closest('.js-sidebar-menu-wrapper').toggleClass('is-active');
	});
	
	$('.js-types-trigger').on('click', function() {
		$(this).closest('.js-types-wrapper').toggleClass('is-active');
	});
	$('.js-types-trigger2').on('click', function() {
		$(this).closest('.js-types-wrapper2').toggleClass('is-active');
	});

	$('.js-chart-status-trigger').on('click', function() {
        $(this).closest('.js-chart-status-wrapper').toggleClass('is-active');
    });
	$('.js-chart-score-trigger').on('click', function() {
        $(this).closest('.js-chart-score-wrapper').toggleClass('is-active');
    });
	
	$('.js-time-trigger').on('click', function() {
		$(this).closest('.js-time-wrapper').toggleClass('is-active');
	});
	
	$('body').on('click', function(e){
		if ($(e.target).closest('.js-sidebar-menu-wrapper, .js-sidebar-menu-trigger').length === 0) {
			$('.js-sidebar-menu-wrapper').removeClass('is-active');
		}
	});

	$('body').on('click', function(e){
		if ($(e.target).closest('.js-types-wrapper, .js-types-trigger').length === 0) {
			$('.js-types-wrapper').removeClass('is-active');
		}
	});
	$('body').on('click', function(e){
		if ($(e.target).closest('.js-types-wrapper2, .js-types-trigger2').length === 0) {
			$('.js-types-wrapper2').removeClass('is-active');
		}
	});

	$('body').on('click', function(e){
		if ($(e.target).closest('.js-chart-status-wrapper, .js-chart-status-trigger').length === 0) {
			$('.js-chart-status-wrapper').removeClass('is-active');
		}
	});
	$('body').on('click', function(e){
		if ($(e.target).closest('.js-chart-score-wrapper, .js-chart-score-trigger').length === 0) {
			$('.js-chart-score-wrapper').removeClass('is-active');
		}
	});

	$('body').on('click', function(e){
		if ($(e.target).closest('.js-time-wrapper, .js-time-trigger').length === 0) {
			$('.js-time-wrapper').removeClass('is-active');
		}
	});
})();