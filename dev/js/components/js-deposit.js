(function(){
    $('.deposit__choose-amount__sum').on('click', function() {
        if ($(this).hasClass('is-selected')) return;

        $(this).closest('.deposit__choose-amount').find('.deposit__choose-amount__sum.is-selected').removeClass('is-selected');
        $(this).addClass('is-selected');
    });  
})();